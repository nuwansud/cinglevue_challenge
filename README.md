# README #

### What is this repository for? ###

* Coding challenge  
* 1.0


### How do I get set up? ###

* Summary of set up

* In windows 
First install mongoDB in your machine and create a database named test.

1. go to the target folder using command line
> cd target
2. Execute the jar file using
> java -jar vue-1.0-SNAPSHOT.jar
3. Access the website using localhost:8080


### About the project ###

This is single page AngularJS application which consumes a REST API.
REST API is built using java and SpringBoot.
The UI of the application is based on googles material design principles.