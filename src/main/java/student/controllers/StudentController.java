package student.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import student.entity.Student;
import student.service.StudentService;

import java.util.List;

/**
 * Created by Nuwan on 6/14/2017.
 */

// REST controller

@RestController
@RequestMapping("/student")
public class StudentController {

    // student service object
    @Autowired
    StudentService ss;

    // re
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Student> getAllStudents()
    {

        if(ss.getStudents().isEmpty()){
            String[] subjects = {"Physics","Maths","Chemestry"};
            Student st1 = new Student("Kamal Perera",22, subjects);
            System.out.println("1 written");
            Student st2 = new Student("Kasun Gunawardane",20, subjects);
            System.out.println("2nd written");

            ss.addStudent(st1);
            ss.addStudent(st2);
        }

        return ss.getStudents();
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addStudent(@RequestBody final Student student){
        ss.addStudent(student);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updateStudent(@RequestBody final Student student){
        ss.addStudent(student);
    }

    @RequestMapping(value = "/delete/{id}",method = RequestMethod.DELETE)
    @Transactional
    public void deleteStudent(@PathVariable("id") String sid)
    {
        ss.deleteStudent(sid);
    }


}
