package student.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import student.dao.StudentRepository;
import student.entity.Student;

import java.util.List;

/**
 * Created by Nuwan on 6/14/2017.
 */


//service layer

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepo;


    public List<Student> getStudents()
    {
        return studentRepo.findAll();
    }

    public void addStudent(Student st)
    {
        studentRepo.save(st);
    }

    public void deleteStudent(String studentId)
    {
        studentRepo.delete(studentId);
    }

}
