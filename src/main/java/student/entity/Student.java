package student.entity;

import org.springframework.data.annotation.Id;

/**
 * Created by Nuwan on 6/13/2017.
 */

//student entity class


public class Student {

    // primary key
  @Id
   private String id;

    private String name;
    private int age;
    private String[] subjects;

    // constructors

    public Student(String id, String name, int age, String[] subjects) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.subjects = subjects;
    }

    public Student(String name, int age, String[] subjects) {
        this.name = name;
        this.age = age;
        this.subjects = subjects;
    }

    public Student()
    {}



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }


}
