package student.dao;



import org.springframework.data.mongodb.repository.MongoRepository;

import student.entity.Student;

import java.util.List;

/**
 * Created by Nuwan on 6/14/2017.
 */

// DAO layer
public interface StudentRepository extends MongoRepository<Student,String> {

    List<Student> findAll();
}
