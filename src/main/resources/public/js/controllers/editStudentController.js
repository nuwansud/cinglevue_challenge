incidentApp.controller('editStudentController',function($scope,$rootScope,message,$mdDialog,$http,$mdToast){
    $scope.studentObj = message;

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.editStudent = function () {
        console.log("edit incident function");
        console.log($scope.incidentObj);
        $http({
            method: 'PUT',
            url: $rootScope.resturl+'/student/update',
            data: $scope.studentObj,
            headers:{
                'Content-Type': 'application/json'}

        }).then(function successCallback(res) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Student updated successfully")
                    .position('top right' )
                    .hideDelay(3000)
            );

            $mdDialog.hide();
        });

    }
});
