incidentApp.controller('navcontroller',['$scope','$rootScope','$location','$mdDialog',function ($scope,$rootScope,$location,$mdDialog) {
    $scope.$on('$viewContentLoaded',function () {
        $('ul.tabs').tabs();

    });

    $scope.logout = function (ev) {

        var originatorEv=ev;

        var confirm = $mdDialog.confirm()
            .title('You are about to logout')
            .textContent('Are you sure ?')
            .targetEvent(originatorEv)
            .ok('OK')
            .cancel('cancel');

        $mdDialog.show(confirm).then(function(result) {

            $rootScope.thisUserId =null;
            $rootScope.thisUserName =null;

            $location.path('login');

        }, function() {

        });



    }

}])
