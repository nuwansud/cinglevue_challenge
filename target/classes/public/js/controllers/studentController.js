incidentApp.controller('studentController', function ($scope, $http, $rootScope, $mdDialog,$mdToast) {

    var originatorEv;

    $scope.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    };

    $scope.$on('$viewContentLoaded', function () {
        $('.collapsible').collapsible({
            accordion: false
        });
    });

    $scope.getStudents = function () {
        $scope.studentObj={};
        var students= $http.get($rootScope.resturl + '/student/all');
        students.then(function (response) {
            $scope.students = response.data;
        });
    }

    $scope.getStudents();


    $scope.addStudent = function (ev) {
        $mdDialog.show({
            controller: 'addStudentController',
            templateUrl: 'views/addStudent.tmpl.htm',
            targetEvent: ev,
            clickOutsideToClose: true,
            parent: angular.element(document.body),
            fullscreen: $scope.customFullscreen,
                    }).then(function () {
            $scope.getStudents();
        });
    }

    $scope.editStudent = function (ev, student) {
       $mdDialog.show({
            controller: 'editStudentController',
            templateUrl: 'views/editStudent.tmpl.htm',
            targetEvent: ev,
            clickOutsideToClose: true,
            parent: angular.element(document.body),
            fullscreen: $scope.customFullscreen,
            locals: {
                message: student
            },
        })
    }
    
    $scope.deleteStudent = function (sid) {
        var confirm = $mdDialog.confirm()
            .title('You are about to delete a student record. Are you sure?')
            .targetEvent(originatorEv)
            .ok('OK')
            .cancel('cancel');

        $mdDialog.show(confirm).then(function(result) {

            $http({
                method: 'DELETE',
                url: $rootScope.resturl+'/student/delete/'+sid,
            }).then(function successCallback(response) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Student record removed!')
                        .position('bottom right')
                        .hideDelay(3000)
                );
                $scope.getStudents();
                });
            });
    }


});
