incidentApp.controller('addStudentController',function($scope,$rootScope,$mdDialog,$http,$mdToast){

    $scope.newStudentObj = {};
    $scope.newStudentObj.subjects=[];

    $scope.cancel = function() {
        $mdDialog.cancel();
    };


    $scope.addNewStudent = function () {

        $http({
            method: 'POST',
            url: $rootScope.resturl+'/student/add',
            data: $scope.newStudentObj,
            headers:{
                'Content-Type': 'application/json'}

        }).then(function successCallback(res) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Student added successfully")
                    .position('top right' )
                    .hideDelay(3000)
            );

            $mdDialog.hide();
        });

    }
});
